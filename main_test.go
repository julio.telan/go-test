package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	//"github.com/gorilla/context"
	"github.com/stretchr/testify/assert"
)

func TestGetIndex(t *testing.T) {
	t.Parallel()

	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()

	//Hack to try to fake gorilla/mux vars
	//vars := map[string]string{
	//	"mystring": "abcd",
	//}
	//context.Set(r, 0, vars)

	Index(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, []byte("Welcome!\n"), w.Body.Bytes())
}